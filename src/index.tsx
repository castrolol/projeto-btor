import * as React from 'react';
import * as ReactDOM from 'react-dom';
import './index.css';

import AppShell from './ui/AppShell';

ReactDOM.render(
  <AppShell />,
  document.getElementById('root') as HTMLElement
);
