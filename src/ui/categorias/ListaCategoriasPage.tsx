import * as React from 'react';
import { Button, Table } from 'antd';
import { RouteComponentProps, withRouter } from 'react-router-dom';

const categorias = [
	{ id: 1, nome: 'Familia' },
	{ id: 2, nome: 'Empresa' },
	{ id: 3, nome: 'Mercado' },
	{ id: 4, nome: 'Carro' },
	{ id: 5, nome: 'Estudos' }
];

function ListaCategoriasPage(props: RouteComponentProps<{}>) {
	const { history } = props;

	return (
		<div>
			<Button type="primary" onClick={() => history.push('/categorias/novo')}>
				{' '}
				Novo{' '}
			</Button>
			<CategoriasTable />
		</div>
	);
}

function _CategoriasTable(props: RouteComponentProps<{}>) {
	const { history } = props;
	console.log(props);

	return (
		<Table
			onRowClick={(ev, index) => history.push(`/categorias/${categorias[index].id}`)}
			dataSource={categorias}
			columns={[
				{
					key: 'id',
					dataIndex: 'id',
					title: '#'
				},
				{
					key: 'nome',
					dataIndex: 'nome',
					title: 'Nome'
				}
			]}
		/>
	);
}

const CategoriasTable = withRouter(_CategoriasTable);

export default ListaCategoriasPage;
