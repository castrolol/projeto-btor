import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';

interface Props extends RouteComponentProps<{ id: number }> {}

function CategoriaPage(props: Props) {
	const { match } = props;

	const id = match.params.id;

	return <h1> Oi, eu sou a CategoriaPage ({id ? `#${id}` : 'novo'}) </h1>;
}

export default CategoriaPage;
