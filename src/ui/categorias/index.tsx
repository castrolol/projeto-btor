import * as React from 'react';
import { Switch, Route } from 'react-router-dom';
import ListaCategoriasPage from './ListaCategoriasPage';
import CategoriaPage from './CategoriaPage';

function Categorias() {
	return (
		<Switch>
			<Route exact path="/categorias/" component={ListaCategoriasPage} /> 
			<Route path="/categorias/novo" component={CategoriaPage} /> 
			<Route path="/categorias/:id" component={CategoriaPage} /> 
		</Switch>
	);
}

export default Categorias;
