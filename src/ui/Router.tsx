import * as React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import Home from './home';
import Transacoes from './transacoes';
import Contas from './contas';
import Categorias from './categorias';
import Login from './login';
import ErrorsRouter from './errors';

class Router extends React.Component {
	render() {
		return (
			<Switch>
				<Route exact path="/" component={Home} />
				<Route path="/transacoes" component={Transacoes} />
				<Route path="/contas" component={Contas} />
				<Route path="/categorias" component={Categorias} />
				<Route path="/login" component={Login} />
				<Route path="/error" component={ErrorsRouter} />
				<Redirect to="/error/404" />
			</Switch>
		);
	}
}

export default Router;
