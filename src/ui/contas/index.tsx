import * as React from 'react';
import { Switch, Route } from 'react-router-dom';
import ListaContasPage from './ListaContasPage';
import ContaPage from './ContaPage';

function Contas() {
	return (
		<Switch>
			<Route exact  path="/contas/" component={ListaContasPage} /> 
			<Route path="/contas/novo" component={ContaPage} /> 
			<Route path="/contas/:id" component={ContaPage} /> 
		</Switch>
	);
}

export default Contas;
