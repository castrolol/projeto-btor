import * as React from 'react';
import { RouteComponentProps } from 'react-router';

interface Props extends RouteComponentProps<{ id: number }> {}

class ContaPage extends React.Component<Props> {

	render() {
		const { match } = this.props;

		const id = match.params.id;

		return <h1> Oi, eu sou a ContaPage ({id ? `#${id}` : 'novo'}) </h1>;
	}
}

export default ContaPage;
