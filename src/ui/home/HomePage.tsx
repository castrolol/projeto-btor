import * as React from 'react';
import { Button } from 'antd';
import { connect, Dispatch } from 'react-redux';

interface Props {
    counter: number;
    add: () => {};
    sub: () => {};
}

function HomePage(props: Props) {
	return (
		<div>
			<h1>Oi, eu sou a HomePage</h1>

			<Button type="dashed" onClick={props.sub}> - </Button>
			<span style={{ fontSize: 40, padding: 16 }}>{props.counter}</span>
			<Button type="dashed" onClick={props.add} > + </Button>
		</div>
	);
}

const mapStateToProps = (state: any, props: any) => {
	return {
		counter: state.counter
	};
};

const mapDispatchToProps = (dispatch: Dispatch<any>, props: any) => ({
	add() {
 		dispatch({
			type: 'COUNTER_ADD'
		});
    },
    sub() {
		dispatch({
			type: 'COUNTER_SUBTRACT'
		});
	},
});

export default connect(
    mapStateToProps, 
    mapDispatchToProps
)(HomePage);
