import * as React from 'react';
import { Switch, Route } from 'react-router-dom';
import ListaTransacoesPage from './ListaTransacoesPage';
import TransacaoPage from './TransacaoPage';

function Transacoes() {
	return (
		<Switch>
			<Route exact path="/transacoes/" component={ListaTransacoesPage} /> 
			<Route path="/transacoes/novo" component={TransacaoPage} /> 
			<Route path="/transacoes/:id" component={TransacaoPage} /> 
		</Switch>
	);
}

export default Transacoes;
