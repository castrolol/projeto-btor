import * as React from 'react';
import { BrowserRouter, NavLink } from 'react-router-dom';
import { Provider } from 'react-redux';
import Router from './Router';
import store from '../infra/store';

class AppShell extends React.Component {
	render() {
		return (
			<Provider store={store} >
				<BrowserRouter>
					<React.Fragment>
						<div>
							<NavLink exact to="/">
								Home
							</NavLink>
							<NavLink to="/transacoes">Transações</NavLink>
							<NavLink to="/contas">Contas</NavLink>
							<NavLink to="/categorias">Categorias</NavLink>
						</div>

						<Router />
					</React.Fragment>
				</BrowserRouter>
			</Provider>
		);
	}
}

export default AppShell;
