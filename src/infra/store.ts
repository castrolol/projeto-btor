import { createStore, Action, applyMiddleware } from 'redux';
import { createLogger } from 'redux-logger';

interface State {
	counter: number;
}

function reducer(state: State = { counter: 0 }, action: Action): State {
	if (action.type == 'COUNTER_ADD') {
		return { counter: state.counter + 1 };
	}

	if (action.type == 'COUNTER_SUBTRACT') {
		return { counter: state.counter - 1 };
	}

	return state;
}

const store = createStore(reducer, applyMiddleware(createLogger()));

export default store;
